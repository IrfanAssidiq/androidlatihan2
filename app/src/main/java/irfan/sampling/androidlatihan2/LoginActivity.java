package irfan.sampling.androidlatihan2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText et1, et2, et3, et4, et5;
    Button btn1;
    String email, nohp;
    int nosepatu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);
        et5 = findViewById(R.id.et5);
        btn1 = findViewById(R.id.btn1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama;
                String password;
                nama = et1.getText().toString();
                password = et2.getText().toString();
                email = et3.getText().toString();
                nohp = et4.getText().toString();
                nosepatu = Integer.parseInt(et5.getText().toString());

                if(nama.equals("") || password.equals("")){
                    Toast.makeText(LoginActivity.this, "username atau password kosong",
                            Toast.LENGTH_SHORT).show();
                }else{
                    if(!nama.equals("budi") || !password.equals("budi123")){
                        Toast.makeText(LoginActivity.this, "username atau password salah",
                                Toast.LENGTH_SHORT).show();
                    }else{
//                        final AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
//                        dialog.setTitle("Welcome").setCancelable(false).
//                        setMessage("Hai "+nama+" Passwordmu adalah "+password).
//                        setPositiveButton("Oke", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                //
//                            }
//                        }).show();
//
//
//                        Toast.makeText(LoginActivity.this, "name = "+nama
//                                +" password = "+password, Toast.LENGTH_SHORT).show();

                        Bundle b = new Bundle();
                        b.putString("nama", nama);
                        b.putString("email", email);
                        b.putString("nohp", nohp);
                        b.putInt("nosepatu", nosepatu);
                        Intent i = new Intent(LoginActivity.this, LoginBerhasil.class);
                        i.putExtras(b);
                        // startActivityForResult(new Intent(LoginActivity.this, LoginBerhasil.class)
                        //, 5000, b);
                        startActivity(i);
                        finish();
                    }
                }
            }
        });
    }
}

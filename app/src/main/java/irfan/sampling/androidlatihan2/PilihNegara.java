package irfan.sampling.androidlatihan2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class PilihNegara extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listnegara);
        Switch swt = findViewById(R.id.switch1);
        final Spinner spn = findViewById(R.id.spinner1);
        final TextView tv_ = findViewById(R.id.tv_negara);

        swt.setChecked(false);
        if(!swt.isChecked()){
            spn.setVisibility(View.GONE);
            tv_.setVisibility(View.GONE);
        }else{
            spn.setVisibility(View.VISIBLE);
            tv_.setVisibility(View.VISIBLE);
        }

        swt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    spn.setVisibility(View.VISIBLE);
                    tv_.setVisibility(View.VISIBLE);
                }else{
                    spn.setVisibility(View.GONE);
                    tv_.setVisibility(View.GONE);
                }
            }
        });

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String x = String.valueOf(spn.getSelectedItem());
                tv_.setText(x);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                tv_.setText("");
            }
        });
    }
}

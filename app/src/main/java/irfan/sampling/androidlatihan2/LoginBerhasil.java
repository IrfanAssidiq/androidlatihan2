package irfan.sampling.androidlatihan2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class LoginBerhasil  extends AppCompatActivity {
    TextView tv1, tv_email, tv_nohp, tv_sepatu;
    String x, y, z;
    int z1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_berhasil);
        tv1 = findViewById(R.id.tv1);
        tv_email = findViewById(R.id.tv_email);
        tv_nohp = findViewById(R.id.tv_nohp);
        tv_sepatu = findViewById(R.id.tv_sepatu);

        Bundle b = getIntent().getExtras();
        if(b != null){
            x = b.getString("nama");
            y = b.getString("email");
            z = b.getString("nohp");
            z1 = b.getInt("nosepatu");
            tv1.setText(x);
            tv_email.setText(y);
            tv_nohp.setText(z);
            tv_sepatu.setText("No. sepatu : "+ z1);

        }else{
            Toast.makeText(getApplicationContext(), " data kosong", Toast.LENGTH_SHORT).show();
            tv1.setText("Kosong");
            tv_email.setText("Kosong");
            tv_nohp.setText("Kosong");
            tv_sepatu.setText("Kosong");

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pilihan_, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.pilihan1 : {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginBerhasil.this);
                builder.setTitle("Warning")
                        .setCancelable(false)
                        .setMessage("Apakah Anda Yakin Ingin Keluar?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent x = new Intent(LoginBerhasil.this,
                                        LoginActivity.class);
                                startActivity(x);
                                finish(); }})
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //
                            }}).show();
            }break;
            case R.id.pilihan2:{
                startActivity(new Intent(LoginBerhasil.this, ActivitySatu.class));
            }break;
            case R.id.pilihan3:{
                startActivity(new Intent(LoginBerhasil.this, PilihNegara.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
